import sys
import re
import math

def vectormagnitude(vector):
    dummy = 0
    for dim in vector:
        dummy += dim**2
    return math.sqrt(dummy)

def vectorangle(vectorA, vectorB):
    dotProd = 0
    for a,b in zip(vectorA, vectorB):
        dotProd += a*b
    
    mod = vectormagnitude(vectorA)*vectormagnitude(vectorB)
    angle = dotProd/mod
    return math.degrees(math.acos(angle))

def PBCfromvectors(vectors):
    x = vectormagnitude(vectors[0])
    y = vectormagnitude(vectors[1])
    z = vectormagnitude(vectors[2])

    #alpha angle between b and c
    a = vectorangle(vectors[1], vectors[2])
    #beta angle between a and c
    b = vectorangle(vectors[0], vectors[2])
    #gamma angle between a and b
    c = vectorangle(vectors[0], vectors[1])

    return x, y, z, a, b, c

if len(sys.argv)==2:
    with open(sys.argv[1], "r") as file, open(sys.argv[1].split(".")[0]+".pdb", "w") as pdb:
        title = file.readline()
        steps = file.read().split(title)
        for stepnum, step in enumerate(steps):
            #READ LATTICE (PBC) FROM XDATCAR
            lines = step.split("\n")
            scalingfactor = lines[0]
            
            #lattice vectors
            vectors = []
            for rawvect in lines[1:4]:
                vectors.append(tuple(map(float, re.findall(r"[-+]?(?:\d*\.\d+|\d+)", rawvect))))

            #WRITE STEP TITLE (PBC) TO PDB
            #print("CRYST1    6.312    6.312    6.316  90.00  90.00  90.00 P 1           1")
            x, y, z, a, b, c = PBCfromvectors(vectors)
            pdb.write(f"CRYST1 {x:>8.3f} {y:>8.3f} {z:>8.3f} {a:>6.2f} {b:>6.2f} {c:>6.2f} P 1           1\n")
            pdb.write(f"MODEL {stepnum:>8}\n")

            #atoms
            atoms = lines[4].split()
            atomcount = map(int, lines[5].split())
            atomlist = list()
            for atom, count in zip(atoms, atomcount):
                for i in range(count):
                    atomlist.append(atom)

            #coords
            for i, (atom, coord) in enumerate(zip(atomlist, lines[7:])):
                #STEP COORDS TO PDB
                coord = tuple(map(float, re.findall(r"[-+]?(?:\d*\.\d+|\d+)", coord)))
                resname="cry"
                pdb.write(f"ATOM  {i:>5} {atom:>4} {resname:>3}          {x*coord[0]:>8.3f}{y*coord[1]:>8.3f}{z*coord[2]:>8.3f}  1.00  0.00          {atom:>2}\n")

            pdb.write(f"ENDMDL\n")
            
            

else:
    print("Invalid Usage, use: XDATCAR-PBD {XDATCAR}")
    exit()