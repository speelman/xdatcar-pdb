Simple utility to transform a vasp XDATCAR into a rudimentary PDB file.
Useful for visualization in VMD with periodic boundary conditions.
